package me.zubear.apps.nyclivebus;

/**
 * Created by Extreme on 4/13/2018.
 */

public class MapDataLayers {
    public final static byte LAYER_BRONX_BUS = 0b1;
    public final static byte LAYER_BROOKLYN_BUS = 0b10;
    public final static byte LAYER_MANHATTAN_BUS = 0b100;
    public final static byte LAYER_QUEENS_BUS = 0b1000;
    public final static byte LAYER_STATENISLAND_BUS = 0b10000;
    public final static byte LAYER_NYC_EXPRESS_BUS = 0b100000;
    public final static byte LAYER_SUBWAY = 0b1000000;
    // Metro-North, LIRR ... maybe at some point?
}