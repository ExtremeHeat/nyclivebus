package me.zubear.apps.nyclivebus.backend;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Extreme on 4/3/2018.
 */

public class VehicleJourneyObject {
    public String LineRef;
    public boolean DirectionRef;
    public String JourneyPatternRef;
    public String PublishedLineName;
    public String OriginRef;
    public String DestinationRef;
    public String DestinationName;
    public double Bearing;
    public String ProgressRate;
    public String BlockRef;
    public String VehicleRef;

    public LatLng VehicleLocation;

    // at ot approaching stop
    public String StopPointRef;
    public int VisitNumber;
    public String StopPointName;
}