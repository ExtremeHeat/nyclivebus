package me.zubear.apps.nyclivebus.backend;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Extreme on 4/3/2018.
 */

public class LiveVehicleDataPoller {
    public static String API_BACKEND_URL = "http://192.168.1.3/api/nyclivebus/getCurrentBusLocationsByRoute.php?route=";

    public static String API_FALLBACK_URL = "https://bustime.mta.info/api/siri/vehicle-monitoring.json?key=OBANYC&_=1522767392824&OperatorRef=MTA+NYCT&LineRef=";

    public static String get(URL url) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setDoOutput(true);
        connection.setConnectTimeout(5000);
        connection.setReadTimeout(5000);
        connection.connect();
        BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String content = "", line;
        while ((line = rd.readLine()) != null) {
            content += line + "\n";
        }
        return content;
    }

    public static JSONObject getJsonDataForRoute(String routeId) {
        try {
            if (true) throw new Exception("Temporary exception to force fallback, disregard"); // TODO: REMOVE, TEMPORARY FOR TESTING
            String r = get(new URL(API_BACKEND_URL + routeId));
            JSONArray json = new JSONArray(r);
            for (int i = 0; i < json.length(); i++) {
                JSONObject siriObject = json.getJSONObject(i);
                return siriObject;
            }
            System.out.println("Fetched response from server: " + r);
        } catch (Exception e) {
            try {
                String line = routeId.toUpperCase().replace("-SBS", "%2B");
                String r = get(new URL(API_FALLBACK_URL + line));
                JSONObject siriObject = new JSONObject(r);
                return siriObject;
            } catch (IOException ex) {
                ex.printStackTrace();
                return null;
            } catch (JSONException ex) {
                ex.printStackTrace();
                return null;
            }
        }
        return null;
    }
}