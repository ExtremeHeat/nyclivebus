package me.zubear.apps.nyclivebus;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Color;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.data.geojson.GeoJsonFeature;
import com.google.maps.android.data.geojson.GeoJsonLayer;
import com.google.maps.android.data.geojson.GeoJsonLineString;
import com.google.maps.android.data.geojson.GeoJsonLineStringStyle;
import com.google.maps.android.data.geojson.GeoJsonPointStyle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Extreme on 4/2/2018.
 */

public class RouteLoader {

    Resources resources;

    GoogleMap map;

    Activity activity;

    public static HashMap<String, Integer> routeColorMap = new HashMap<>();

    public RouteLoader(GoogleMap map, Resources resources, Activity activity) {
        System.out.println("Loading bxbusrt1.json...");
        this.resources = resources;
        this.map = map;
        this.activity = activity;
    }

    public void loadRoutes(int routeResourceId, int routeColorsResourceId) throws IOException, JSONException {
        System.out.println("LOADING JSON.");
        //String routeJson = loadJsonRaw(routeResourceId);
        System.out.println("LOADED JSON.");
        JSONObject mainObject = new JSONObject(loadJsonRaw(routeResourceId));
        System.out.println("LOADED JSON OBJECT");

        JSONObject routeColorsObject = new JSONObject(loadJsonRaw(routeColorsResourceId));
        System.out.println("LOADED ROUTE COLORS");
        for (int i = 0; i < mainObject.names().length(); i++) {
            String route = mainObject.names().getString(i);
            String c = routeColorsObject.getString(route);
            int color;
            switch (c) {
                case "lightblue":
                    color = Color.CYAN;
                    break;
                case "orange":
                    color = Color.YELLOW;
                    break;
                case "red":
                    color = Color.RED;
                    break;
                case "blue":
                    color = Color.BLUE;
                    break;
                case "purple":
                    color = Color.MAGENTA;
                    break;
                case "green":
                    color = Color.GREEN;
                    break;
                default:
                    color = Color.BLACK;
            }
            routeColorMap.put(route, color);
            addRoute(loadRouteLines(mainObject.getJSONObject(route)), route, color);
        }
    }

    public GeoJsonLayer loadSubwayLines(int resourceId, String routes) throws IOException, JSONException {
        JSONObject subwayGeoJson = new JSONObject(loadJsonRaw(resourceId));
        JSONArray features = subwayGeoJson.getJSONArray("features");

        final GeoJsonLayer layer = new GeoJsonLayer(map, new JSONObject());
        GeoJsonLineStringStyle pointStyle = layer.getDefaultLineStringStyle();
        pointStyle.setZIndex(-1);
        pointStyle.setWidth(4);

        for (int i = 0; i < features.length(); i++) {
            JSONObject feature = features.getJSONObject(i);
            if (feature.getString("type").equals("Feature")) {
                JSONObject jprops = feature.getJSONObject("properties");
                HashMap<String, String> properties = new HashMap<>();
                properties.put("name", jprops.getString("name"));
                properties.put("rt_symbol", jprops.getString("rt_symbol"));
                properties.put("objectid", jprops.getString("objectid"));
                properties.put("id", jprops.getString("id"));
                properties.put("shape_len", jprops.getString("shape_len"));

                if (!routes.contains(properties.get("rt_symbol"))) {
                    continue;
                }

                JSONObject geometry = feature.getJSONObject("geometry");

                if (geometry.getString("type").equals("LineString")) {
                    JSONArray jcoords = geometry.getJSONArray("coordinates");
                    List<LatLng> coordinates = new ArrayList<>();
                    for (int j = 0; j < jcoords.length(); j++) {
                        JSONArray jcoord = jcoords.getJSONArray(j);
                        LatLng latlng = new LatLng(jcoord.getDouble(1), jcoord.getDouble(0));
                        coordinates.add(latlng);
                    }
                    GeoJsonLineString lineString = new GeoJsonLineString(coordinates);

                    final GeoJsonFeature pointFeature = new GeoJsonFeature(lineString, properties.get("id"),
                            properties, null);
                    layer.addFeature(pointFeature);
                }
            }
        }
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                layer.addLayerToMap();
            }
        });

        return layer;
    }

    private List<List<LatLng>> loadRouteLines(JSONObject routeObject) throws JSONException {
        JSONArray polylineContainer = routeObject.getJSONArray("seq");
        JSONArray polylineSet = polylineContainer.getJSONArray(0);

        List<List<LatLng>> polylines = new ArrayList<>();

        int n = 0;

        for (int i = 0; i < polylineSet.length(); i++) {
            JSONArray polyline = polylineSet.getJSONArray(i);
            //polylines.add(i, new ArrayList<List<LatLng>>());
            polylines.add(n, new ArrayList<LatLng>());
            for (int j = 0; j < polyline.length(); j++) {
                JSONArray polylinePoints = polyline.getJSONArray(j);
                //polylines.get(i).add(new ArrayList<LatLng>());
                for (int k = 0; k < polylinePoints.length(); k++) {
                    JSONArray polylinePoint = polylinePoints.getJSONArray(k);
                    String lat = polylinePoint.getString(0);
                    String lng = polylinePoint.getString(1);
                    polylines.get(n).add(new LatLng(Double.valueOf(lat), Double.valueOf(lng)));
                    //System.out.println(String.format("Found latlng: %s, %s", lat, lng));
                }
            }
            n++;
        }

        return polylines;
    }

    private void addRoute(final List<List<LatLng>> polyarr, final String routeName, final int color) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for (List<LatLng> p : polyarr) {
                    map.addPolyline(new PolylineOptions().width(5).color(color).addAll(p).clickable(true)).setTag(new String(routeName));
                }
            }
        });
    }

    private String loadJsonRaw(int resourceId) throws IOException {
        InputStream is = resources.openRawResource(resourceId);
        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();
        String json = new String(buffer, "UTF-8");
        return json;
    }

}