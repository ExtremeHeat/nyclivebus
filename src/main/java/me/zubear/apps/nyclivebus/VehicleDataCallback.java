package me.zubear.apps.nyclivebus;

import java.util.List;

import me.zubear.apps.nyclivebus.backend.VehicleJourneyObject;

/**
 * Created by Extreme on 4/6/2018.
 */

public interface VehicleDataCallback {
    void run(List<VehicleJourneyObject> vehicleJourneyObjectList);
}
