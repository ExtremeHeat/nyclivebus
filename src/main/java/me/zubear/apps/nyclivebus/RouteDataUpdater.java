package me.zubear.apps.nyclivebus;

import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import me.zubear.apps.nyclivebus.MapsActivity;
import me.zubear.apps.nyclivebus.VehicleDataCallback;
import me.zubear.apps.nyclivebus.backend.VehicleJourneyObject;

/**
 * Created by Extreme on 4/6/2018.
 */

public class RouteDataUpdater {
    private Handler mHandler = new Handler(Looper.getMainLooper());

    private Runnable mStatusChecker;
    private int UPDATE_INTERVAL = 5000;

    public RouteDataUpdater(final MapManager mapManager) {
        mStatusChecker = new Runnable() {
            @Override
            public void run() {

                for (String line : mapManager.trackedLines) {
                    VehicleProgressLoader.getVehiclePositionsForRoute(line, new VehicleDataCallback() {
                        @Override
                        public void run(final List<VehicleJourneyObject> vehicleJourneyObjects) {

                            System.out.println("Ticking RouteDataUpdater: " + mapManager.trackedLines);

                            mapManager.getBaseActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    for (Marker marker : mapManager.trackedMarkers) {
                                        marker.remove();
                                    }
                                    mapManager.trackedMarkers.clear();

                                    for (VehicleJourneyObject vehicleJourneyObject : vehicleJourneyObjects) {
                                        Marker marker = mapManager.addVehicleMarker(vehicleJourneyObject);

                                        mapManager.trackedMarkers.add(marker);
                                    }
                                    Toast.makeText(mapManager.getBaseActivity(), "Updated!", Toast.LENGTH_SHORT);
                                    //polyline.setColor(Color.BLACK);
                                }
                            });
                        }
                    });
                }

                mHandler.postDelayed(this, UPDATE_INTERVAL);
            }
        };
    }

    public synchronized void startUpdates(){
        mStatusChecker.run();
    }

    public synchronized void stopUpdates(){
        mHandler.removeCallbacks(mStatusChecker);
    }
}
