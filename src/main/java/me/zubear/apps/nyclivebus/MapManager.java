package me.zubear.apps.nyclivebus;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.maps.android.data.Feature;
import com.google.maps.android.data.Layer;
import com.google.maps.android.data.geojson.GeoJsonLayer;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import me.zubear.apps.nyclivebus.backend.VehicleJourneyObject;

/**
 * Created by Extreme on 4/13/2018.
 */

public class MapManager {

    protected final MapsActivity mMapsActivity;
    protected final GoogleMap mMap;

    private RouteLoader routeLoader;

    private Polyline mCurrentlySelectedLine = null;

    protected List<String> trackedLines = new ArrayList<>();
    protected List<Marker> trackedMarkers = new ArrayList<>();

    protected GeoJsonLayer subwayLayer = null;

    private RouteDataUpdater routeDataUpdater;
    protected VehicleProgressLoader vehicleProgressLoader;

    protected float mapLastZoomLevel = 0;

    public MapManager(MapsActivity mapActivity, GoogleMap map) {
        this.mMapsActivity = mapActivity;
        this.mMap = map;
        this.routeDataUpdater = new RouteDataUpdater(this);
    }

    public boolean load() {
        System.out.println("STARTING LOADING SEQUENCE.... on thread " + Thread.currentThread().getName());
        routeLoader = new RouteLoader(mMap, mMapsActivity.getResources(), getBaseActivity());
        int layers = 0;
        layers |= MapDataLayers.LAYER_BRONX_BUS;
        try {
            loadDataLayers(layers);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }

        System.out.println("LOADED ROUTES.");

        // Run on UI thread ... this can block
        mMapsActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                registerPolylineClickHandler();
                registerMapClickHandler();
                registerMapMoveHandler();
            }
        });
        // Stupid Android garbage collector
        System.gc();
        //System.exit(0);

        startMapDataUpdater();

        return true;
    }

    public MapsActivity getBaseActivity() {
        return mMapsActivity;
    }

    public void reset() {
        stopMapDataUpdater();
        mMap.clear();
    }

    public void loadDataLayers(int layers) throws IOException, JSONException {
        if ((layers & MapDataLayers.LAYER_BRONX_BUS) > 0) {
            loadBronxBusLayer();
        }
        if ((layers & MapDataLayers.LAYER_BROOKLYN_BUS) > 0) {
            loadManhattanBusLayer();
        }
        if ((layers & MapDataLayers.LAYER_MANHATTAN_BUS) > 0) {
            loadManhattanBusLayer();
        }
        if ((layers & MapDataLayers.LAYER_QUEENS_BUS) > 0) {
            loadQueensBusLayer();
        }
        if ((layers & MapDataLayers.LAYER_STATENISLAND_BUS) > 0) {
            loadStatenBusLayer();
        }

        loadCitywideSubwayLayer2();
    }

    private void loadBronxBusLayer() throws IOException, JSONException {
        routeLoader.loadRoutes(R.raw.bxbusrt1, R.raw.bxbuscols);
    }

    private void loadBrooklynBusLayer() {}

    private void loadManhattanBusLayer() {

    }

    private void loadQueensBusLayer() {}

    private void loadStatenBusLayer() {}

    private void loadCitywideSubwayLayer() {
        try {
            final GeoJsonLayer layer = new GeoJsonLayer(mMap, R.raw.nycsubway, getBaseActivity());
            mMapsActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    layer.addLayerToMap();
                }
            });
            this.subwayLayer = layer;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void loadCitywideSubwayLayer2() {
        try {
            subwayLayer = routeLoader.loadSubwayLines(R.raw.nycsubway, "123456D");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void clearCurrentlySelectedPolyline() {
        if (mCurrentlySelectedLine == null) return;
        mCurrentlySelectedLine.setWidth(5);
        mCurrentlySelectedLine.setZIndex(1);
        mCurrentlySelectedLine = null;
    }

    public Marker addVehicleMarker(VehicleJourneyObject vehicleJourneyObject) {
        Integer color = RouteLoader.routeColorMap.get(vehicleJourneyObject.PublishedLineName);
        int vehRes = R.drawable.vehicle_0;
        switch (color) {
            case Color.RED:
                vehRes = R.drawable.vehicle_red_0;
                break;
            case Color.GREEN:
                vehRes = R.drawable.vehicle_green_0;
                break;
            case Color.YELLOW:
                vehRes = R.drawable.vehicle_orange_0;
                break;
            case Color.BLUE:
                vehRes = R.drawable.vehicle_blue_0;
                break;
            case Color.CYAN:
                vehRes = R.drawable.vehicle_cyan_0;
                break;
            case Color.MAGENTA:
                vehRes = R.drawable.vehicle_purple_0;
                break;
            default:
                vehRes = R.drawable.vehicle_white_0;
                break;
        }

        Marker marker = mMap.addMarker(new MarkerOptions()
                .position(vehicleJourneyObject.VehicleLocation)
                .anchor(0.5f, 0.5f)
                .title(vehicleJourneyObject.PublishedLineName + " to " + vehicleJourneyObject.DestinationName)
                .snippet(String.format("Vehicle ID: %s, Bearing: %4.3f", vehicleJourneyObject.VehicleRef, vehicleJourneyObject.Bearing))
                .icon(BitmapDescriptorFactory.fromResource(vehRes))
                .rotation(((float)(360.0 - vehicleJourneyObject.Bearing)))
                .flat(true)
        );
        marker.setInfoWindowAnchor(0.6f, 0.7f);

        return marker;
    }

    private void registerPolylineClickHandler() {
        mMap.setOnPolylineClickListener(new GoogleMap.OnPolylineClickListener() {
            @Override
            public void onPolylineClick(final Polyline polyline) {
                Toast.makeText(mMapsActivity.getApplicationContext(), (String)polyline.getTag(), Toast.LENGTH_SHORT).show();

                final String lineName = (String)polyline.getTag();

                Feature feature = subwayLayer.getFeature(polyline);
                boolean isSubwayLine = feature != null;

                if (isSubwayLine) { // Subway line
                    clearCurrentlySelectedPolyline();
                } else if (mCurrentlySelectedLine != null) {
                    if (mCurrentlySelectedLine.getTag() == polyline.getTag()) {
                        Intent i = new Intent(mMapsActivity.getBaseContext(), WebActivity.class);
                        i.putExtra("url", "http://bustime.mta.info/m/index?q=" + lineName);
                        mMapsActivity.startActivity(i);
                        return;
                    }
                    clearCurrentlySelectedPolyline();
                }

                polyline.setWidth(16);
                polyline.setZIndex(2);
                mCurrentlySelectedLine = polyline;
                if (trackedMarkers.size() > 0) { // clear any tracked busses if we have them
                    for (Marker marker : trackedMarkers) {
                        marker.remove();
                    }
                }
                trackedLines.clear();

                if (!isSubwayLine)
                VehicleProgressLoader.getVehiclePositionsForRoute(lineName, new VehicleDataCallback() {
                    @Override
                    public void run(final List<VehicleJourneyObject> vehicleJourneyObjects) {
                        mMapsActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                for (Marker marker : trackedMarkers) {
                                    marker.remove();
                                }
                                trackedMarkers.clear();

                                for (VehicleJourneyObject vehicleJourneyObject : vehicleJourneyObjects) {

                                    Marker marker = addVehicleMarker(vehicleJourneyObject);

                                    trackedMarkers.add(marker);
                                    trackedLines.clear();
                                    trackedLines.add(lineName);
                                }
                            }
                        });
                    }
                });
            }
        });
    }

    public void registerMapMoveHandler() {
        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                CameraPosition cameraPosition = mMap.getCameraPosition();
                Log.d("MapManager", "Camera position is: " + cameraPosition.zoom);
                if(cameraPosition.zoom > 18f && mapLastZoomLevel < 18f) {
                    mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                } else if (cameraPosition.zoom < 18f && mapLastZoomLevel > 18f) {
                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                }
                mapLastZoomLevel = cameraPosition.zoom;
            }
        });
    }

    private void registerMapClickHandler() {
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (mCurrentlySelectedLine != null) {
                    clearCurrentlySelectedPolyline();
                    return;
                }
            }
        });
    }

    public void removePolylineClickHandler() {
        mMapsActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMap.setOnPolygonClickListener(null);
            }
        });
    }

    public void removeMapMoveHandler() {
        mMapsActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMap.setOnCameraMoveListener(null);
            }
        });
    }

    public void removeMapClickHandler() {
        mMapsActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMap.setOnMapClickListener(null);
            }
        });
    }

    public void startMapDataUpdater() {
        if (routeDataUpdater != null)
        routeDataUpdater.startUpdates();

    }

    public void stopMapDataUpdater() {
        if (routeDataUpdater != null)
        routeDataUpdater.stopUpdates();
    }
}
