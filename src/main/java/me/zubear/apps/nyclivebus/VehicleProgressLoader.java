package me.zubear.apps.nyclivebus;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import me.zubear.apps.nyclivebus.backend.LiveVehicleDataPoller;
import me.zubear.apps.nyclivebus.backend.VehicleJourneyObject;

/**
 * Created by Extreme on 4/3/2018.
 */

public class VehicleProgressLoader {
    public static List<VehicleJourneyObject> getVehicleJourneys(JSONArray data) {
        List<VehicleJourneyObject> vehicleJourneyObjects = new ArrayList<VehicleJourneyObject>();
        for (int i = 0; i < data.length(); i++) {
            try {
                JSONObject vehicleJourney = data.getJSONObject(i);
                if (vehicleJourney.has("ErrorCondition")) {
                    return vehicleJourneyObjects;
                }
                JSONArray vehicleActivities = vehicleJourney.getJSONArray("VehicleActivity");
                for (int v = 0; v < vehicleActivities.length(); v++) {
                    JSONObject vehicleActivity = vehicleActivities.getJSONObject(v);
                    JSONObject vehicleStatus = vehicleActivity.getJSONObject("MonitoredVehicleJourney");
                    VehicleJourneyObject vehicleJourneyObject = new VehicleJourneyObject();
                    vehicleJourneyObject.LineRef = vehicleStatus.getString("LineRef");
                    vehicleJourneyObject.DirectionRef = vehicleStatus.getString("DirectionRef") == "1";
                    vehicleJourneyObject.JourneyPatternRef = vehicleStatus.getString("JourneyPatternRef");
                    vehicleJourneyObject.PublishedLineName = vehicleStatus.getString("PublishedLineName");
                    vehicleJourneyObject.OriginRef = vehicleStatus.getString("OriginRef");
                    vehicleJourneyObject.DestinationRef = vehicleStatus.getString("DestinationRef");
                    vehicleJourneyObject.DestinationName = vehicleStatus.getString("DestinationName");
                    vehicleJourneyObject.Bearing = vehicleStatus.getDouble("Bearing");
                    vehicleJourneyObject.ProgressRate = vehicleStatus.getString("ProgressRate");
                    //vehicleJourneyObject.BlockRef = vehicleStatus.getString("BlockRef");
                    vehicleJourneyObject.VehicleRef = vehicleStatus.getString("VehicleRef");
                    JSONObject vloc = vehicleStatus.getJSONObject("VehicleLocation");
                    vehicleJourneyObject.VehicleLocation = new LatLng(vloc.getDouble("Latitude"), vloc.getDouble("Longitude"));
                    vehicleJourneyObjects.add(vehicleJourneyObject);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return vehicleJourneyObjects;
    }

    public static List<LatLng> getVehiclePositionsForRoute(final String routeId, final VehicleDataCallback callback) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    JSONObject data = LiveVehicleDataPoller.getJsonDataForRoute(routeId);
                    List<VehicleJourneyObject> vehicleJourneyObjects;

                    if (data == null) {
                        System.err.println("Failed to get positions for route " + routeId);
                        return;
                    }

                    JSONArray vehicleMonitoring = data.getJSONObject("Siri").getJSONObject("ServiceDelivery").getJSONArray("VehicleMonitoringDelivery");
                    vehicleJourneyObjects = getVehicleJourneys(vehicleMonitoring);
                    callback.run(vehicleJourneyObjects);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            };
        });

        thread.start();

        List<LatLng> l = new ArrayList<LatLng>();
        return l;
    }
}
