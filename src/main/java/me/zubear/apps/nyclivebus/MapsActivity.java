package me.zubear.apps.nyclivebus;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.squareup.leakcanary.RefWatcher;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import me.zubear.apps.nyclivebus.backend.VehicleJourneyObject;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    MapManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    public GoogleMap getGoogleMap() {
        return mMap;
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Move to NYC
        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(40.8583, -73.8876)));
        mMap.setMinZoomPreference(10);

        manager = new MapManager(this, mMap);

        Thread sowedontblockthemainthread = new Thread(new Runnable() {
            @Override
            public void run() {
                manager.load();
            }
        });

        sowedontblockthemainthread.start();
    }

    public void onDestroy() {
        super.onDestroy();

        RefWatcher refWatcher = NYCLiveBusApplication.getRefWatcher(this);
        refWatcher.watch(this);
    }
}
